FROM node:9.2.0-alpine
COPY index.js /index.js
CMD ["node", "index.js"]